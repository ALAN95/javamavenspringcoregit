package org.example;

import org.springframework.beans.factory.annotation.Value;

public class Mundo {

    // La anotacion @Vale("valorPorDefecto") permite agregar un valor a la varible
    @Value("Hola Mundo")
    private String saludo;

    public String getSaludo() {
        return saludo;
    }

    public void setSaludo(String saludo) {
        this.saludo = saludo;
    }
}
