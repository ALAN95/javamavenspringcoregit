package org.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// La anotación @Configuration indica que es la configuración de spring
@Configuration
public class AppConfig2 {
    // La anotacion @Bean indica que es un bean
    // (la propiedad id="marte" indica el nombre del metodo "marte" y la propiedad class="org.example.Mundo"
    // indica la ruta de la clase que utilizara el bean clase de java "Mundo")
    @Bean
    public Mundo marte(){
        return new Mundo();
    }

}
