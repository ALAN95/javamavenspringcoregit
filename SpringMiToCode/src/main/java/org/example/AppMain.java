package org.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppMain {
    public static void main(String[] args) {
        // 1° Metodo: se carga el archivo xml de configuración de la definicion del bean
        /* ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");*/
        // 2° Metodo: con la clase "AnnotationConfigApplicationContext" se le pasa por parametro las clases donde estan definidos los beans
        //            en este caso son clases que tiene las annotaciones de "@Configuration" y "@Bean" que reemplaza el archivo xml
        /* ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class, AppConfig2.class);*/

        // 3° Metodo: con la clase AnnotationConfigApplicationContext no se le pasa nada en el argumento
        //            con el metodo register() registra las clases donde esta definidos los beans
        //            se le pasa en el argumento el nombre de la clase donde estan las annotaciones de "@Configuration" y "@Bean"
        //            y con el metodo refresh() construye los bean como sifuera el xml
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.register(AppConfig.class);
        applicationContext.register(AppConfig2.class);
        applicationContext.refresh();

        Mundo saludoMondoTierra = applicationContext.getBean("mundo", Mundo.class);
        Mundo saludoMondoMarte = applicationContext.getBean("marte", Mundo.class);

        System.out.println("saludoMondoTierra.getSaludo() = " + saludoMondoTierra.getSaludo());
        System.out.println("saludoMondoMarte.getSaludo() = " + saludoMondoMarte.getSaludo());

        ((ConfigurableApplicationContext) applicationContext).close();

    }
}