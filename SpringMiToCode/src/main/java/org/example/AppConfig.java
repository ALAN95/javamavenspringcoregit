package org.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// La anotación @Configuration indica que es la configuración de spring
@Configuration
public class AppConfig {

    // La anotacion @Bean indica que es un bean
    // (la propiedad id="mundo" indica el nombre del metodo "mundo" y la propiedad class="org.example.Mundo"
    // indica la ruta de la clase que utilizara el bean clase de java "Mundo")
    @Bean
    public Mundo mundo(){
        return new Mundo();
    }


}
