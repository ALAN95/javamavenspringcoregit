package org.example;

import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBContext;
import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args)  throws Exception {

        SOAPMessageContext messageContext;

        // Create SOAP Connection
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();

        RequestSOAP request = new RequestSOAP();

        // Send SOAP Message to SOAP Server
        String url = "http://ws.cdyne.com/emailverify/Emailvernotestemail.asmx?WSDL";
        SOAPMessage soapResponse = soapConnection.call(request.createSOAPRequest(), url);
        soapConnection.close();
        // print SOAP Response
        System.out.println("Response SOAP Message:");
        soapResponse.writeTo(System.out);

        SOAPBody soapBody = soapResponse.getSOAPBody();
        System.out.println();
        System.out.println("soapBody-ResponseText: " + soapBody.getElementsByTagName("ResponseText").item(0).getTextContent());
        System.out.println("soapBody-ResponseCode: " + soapBody.getElementsByTagName("ResponseCode").item(0).getTextContent());
        System.out.println("soapBody-GoodEmail: " + soapBody.getElementsByTagName("GoodEmail").item(0).getTextContent());

        //System.out.println("Estatus de Response: " + soapResponse.getSOAPPart().getEnvelope().getBody().getFault().getFaultCode());


        //System.out.println(soapResponse.getSOAPPart().getEnvelope().getBody().getFault().getFaultCode());
/*        messageContext = (SOAPMessageContext) soapResponse;
        soapResponse = messageContext.getMessage();
        SOAPEnvelope soapEnv = soapResponse.getSOAPPart().getEnvelope();
        SOAPHeader soapHeader = soapEnv.getHeader();

        int status = (( javax.servlet.http.HttpServletResponse)messageContext.get("HTTP.RESPONSE")).getStatus();
        System.out.println("status = " + status);*/

/*        SOAPBody responseBody = soapResponse.getSOAPBody();
        SOAPBodyElement responseElement = (SOAPBodyElement) responseBody.getChildElements().next();
        SOAPElement returnElement = (SOAPElement) responseElement.getChildElements().next();
        if (responseBody.getFault() != null) {
            System.out.println("fault != null");
            System.out.println(returnElement.getValue() + " " + responseBody.getFault().getFaultString());
        } else {
            String serverResponse = returnElement.getValue();
            System.out.println(serverResponse);
            System.out.println("\nfault == null, got the response properly.\n");
        }*/

    }

}