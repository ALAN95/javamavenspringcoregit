package org.example.spring.annotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotations {
    public static void main(String[] args) {


        // Leer o cargar el xml de configuración
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // Pedir un bean al contenedor
        Empleados Marcos = context.getBean("ComercialExperimentado", Empleados.class);

        // Usar el bean
        System.out.println(Marcos.getInforme());
        System.out.println(Marcos.getTareas());

        // Cerrar el contexto
        context.close();

    }
}
