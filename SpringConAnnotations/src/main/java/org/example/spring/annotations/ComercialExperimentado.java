package org.example.spring.annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

// Con esta annotacion ya hemos creado nuestro bean
// Dentro de la anotación se puede poner el ID para identificar el bean
// Por lo regular se pone el mismo nombre que el de la clase
// Tambien se puede omitir el id del bean, es decir, en la anotacion
// puede quedar "@Component" y entonces spring toma como id el nombre de la clase y
// la primera letra en minuscula "comercialExperimentado"
// En la clase principal "Empleados Marcos = context.getBean("comercialExperimentado", Empleados.class);"
@Component("comercialExperimentado")
//@Scope("prototype")
public class ComercialExperimentado implements Empleados{

    // Con la anotación "@Autowired" permite realizar la inyección de dependencia mediante un campo de clase
    // Con la anotación @Qualifier("") permite especificar que clase realizara la inyección de dependencia,
    // dentro de las comillas va el id (si fuera un archivo xml), es decir, dentro de las comillas va el
    // nombre de la clase que se quiere especificar
    @Autowired
    @Qualifier("informeFinancieroTrim1")
    private CreaccionInformeFinaciero nuevoInforme;

    /*
    @Autowired
    public ComercialExperimentado(CreaccionInformeFinaciero nuevoInforme) {
        this.nuevoInforme = nuevoInforme;
    }*/

    /*
    @Autowired
    public void queMasDaElNombre(CreaccionInformeFinaciero nuevoInforme) {
        this.nuevoInforme = nuevoInforme;
    }*/

    @Override
    public String getTareas() {
        return "Vender, vender y vender mas!";
    }

    @Override
    public String getInforme() {
        //return "Informe generado por el comercial";
        return nuevoInforme.getInformeFinanciero();
    }

    //Ejecución de codigo despues de creacion del Bean
    @PostConstruct
    public void ejecutaDespuesCreaccionBean(){
        System.out.println("Ejecutado tras creacion del BEAN\n");
    }

    // Ejecucion de codigo despues de apagado contenedor Spring
    @PreDestroy
    public void ejecutaAntesDestruccionBean(){
        System.out.println("\nEjecuta antes de la destrucción del BEAN");
    }


}
