package org.example.spring.annotations;

import org.springframework.stereotype.Component;

@Component
public class InformeFinancieroTrim4 implements CreaccionInformeFinaciero{

    @Override
    public String getInformeFinanciero() {
        return "Presentacion de informe de cierre de año 4";
    }
}
