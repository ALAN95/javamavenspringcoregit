package org.example.spring.annotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotationsSingletonPrototype {
    public static void main(String[] args) {

        //ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("applicationContext.xml");
        // Leer el class de configuracion
        AnnotationConfigApplicationContext contexto = new AnnotationConfigApplicationContext(EmpleadosConfig.class);
        Empleados empleados = contexto.getBean("directorFinanciero", Empleados.class);
        System.out.println(empleados.getTareas());
        System.out.println(empleados.getInforme());

        DirectorFinanciero empleadosDirector = contexto.getBean("directorFinanciero", DirectorFinanciero.class);
        System.out.println(empleadosDirector.getEmail());
        System.out.println(empleadosDirector.getNombreEmpresa());


        Empleados Antonio = contexto.getBean("comercialExperimentado", Empleados.class);
        Empleados Lucia = contexto.getBean("comercialExperimentado", Empleados.class);

        // Apuntan al mismo objecto en memoria
        if(Antonio == Lucia){
            System.out.println("PATRON DE DISEÑO: SINGLETON");
            System.out.println("Apuntan al mismo lugar en memoria");
            System.out.println(Antonio + "\n" + Lucia);
        }else{
            System.out.println("PATRON DE DISEÑO: PROTOTYPE");
            System.out.println("No apuntan al mismo lugar en memoria");
            System.out.println(Antonio + "\n" + Lucia);
        }

        contexto.close();

    }
}
