package org.example.spring.annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class DirectorFinanciero implements Empleados{

    private CreaccionInformeFinaciero informeFinanciero;
    @Value("${email}")
    private String email;

    @Value("${nombreEmpresa}")
    private String nombreEmpresa;

    @Autowired
    public DirectorFinanciero (CreaccionInformeFinaciero informeFinanciero){
        this.informeFinanciero = informeFinanciero;
    }

    @Override
    public String getTareas() {
        return "Gestion y direccion de las operaciones de la empresa";
    }

    @Override
    public String getInforme() {
        return informeFinanciero.getInformeFinanciero();
    }

    public String getEmail() {
        return email;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }
}
