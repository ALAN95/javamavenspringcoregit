package org.example.spring.annotations;

import org.springframework.stereotype.Component;

@Component("informeFinancieroTrim2")
public class InformeFinancieroTrim2 implements CreaccionInformeFinaciero{
    @Override
    public String getInformeFinanciero() {
        return "Presentacion de informe catastrofico del trimestre 2";
    }

}
