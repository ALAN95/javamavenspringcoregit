package org.example.spring.annotations;

import org.springframework.stereotype.Component;

@Component("informeFinancieroTrim1")
public class InformeFinancieroTrim1 implements CreaccionInformeFinaciero{

    @Override
    public String getInformeFinanciero() {
        return "Presentacion de informe financiero del trimetre 1";
    }
}
