package org.example.spring.annotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

// Las annotations de @Configuration y @ComponentScan("org.example.spring.annotations") permite
// presindir del archivo xml (applicationContext.xml), es decir, que ya no utilizaremos
// el archivo xml donde se realizaba el scaneo del proyecto para saber donde estan los beans
@Configuration
@ComponentScan("org.example.spring.annotations")//Se le pasa la ruta donde tiene que realizar el scanner de los beans del proyecto
@PropertySource("classpath:datosEmpresa.properties")
public class EmpleadosConfig {

    // Definir el bean para informe InformeFinancieroDtoCompras
    // El nombre del metodo informeFinancieroDtoCompras() es el id del Bean inyectado
    @Bean
    public CreaccionInformeFinaciero informeFinancieroDtoCompras(){
        return new InformeFinancieroDtoCompras();
    }

    // Definir el bean para DirectorFinanciero e inyectar dependencias
    @Bean
    public Empleados directorFinanciero(){
        return new DirectorFinanciero(informeFinancieroDtoCompras());
    }


}
