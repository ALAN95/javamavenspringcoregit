package org.example.spring.annotations;

import org.springframework.stereotype.Component;

@Component
public class InformeFinancieroTrim3 implements CreaccionInformeFinaciero{
    @Override
    public String getInformeFinanciero() {
        return "Presentacion de informe favorable del trimestre 3";
    }
}
