package mx.com.system.qa.spring;

public class JefeEmpleado implements Empleados{

    private CreacionInformes nuevoInforme;

    public JefeEmpleado(CreacionInformes nuevoInforme){
        this.nuevoInforme = nuevoInforme;
    }

    public String getTareas(){
        return "Gestiono las cuestiones relativas de mis empleados de seccion";
    }

    public String getInforme() {
        return "Informe presentado por el Jefe con arreglos: " + nuevoInforme.getInforme();
    }


}
