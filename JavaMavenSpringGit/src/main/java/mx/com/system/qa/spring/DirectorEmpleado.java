package mx.com.system.qa.spring;

public class DirectorEmpleado implements Empleados {
    // Creación de campo tipo CreacionInforme --> interfaz
    private CreacionInformes informeNuevo;
    private String email;
    private String nombreEmpresa;

    // Creación de constructor que inyecta la dependencia
    public DirectorEmpleado(CreacionInformes informeNuevo){
        this.informeNuevo = informeNuevo;
    }

    public String getTareas(){
        return "Gestionar la plantilla de la empresa";
    }

    public String getInforme() {
        return "Informe creado por el Director: " + informeNuevo.getInforme();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    // Metodo init. Ejecutar tareas antes de que el bean este disponible
    public void metodoInicial(){
        // En este metodo irian todo las tareas antes de que el bean este disponible
        // Por ejemplo una conexión a la base de datos, abrir websockets
        System.out.println("Dentro del metodo init. Aqui irian las tareas ejecutar"
                            + "antes de que el bean este listo");
    }

    // Metodo destroy. Ejecutar tareas despues de que el bean haya sido utilizado
    public void metodoFinal(){
        // En este metodo irian todo las tareas antes de que el bean este disponible
        // Por ejemplo una conexión a la base de datos, abrir websockets
        System.out.println("Dentro del metodo destroy. Aqui irian las tareas ejecutar"
                + "despues de utilizar el bean");
    }

}
