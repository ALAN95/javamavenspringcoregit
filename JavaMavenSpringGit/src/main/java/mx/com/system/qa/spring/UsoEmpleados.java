package mx.com.system.qa.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoEmpleados {
    public static void main(String[] args) {


        /*
        // Creacion del objecto de tipo empleado
        Empleados empleado1 = new DirectorEmpleado();

        // Uso de los objectos creados
        String tareaEmpleado = empleado1.getTareas();
        System.out.println(tareaEmpleado); */

        ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("applicationContext.xml");

        // Se crea un objecto de la interfaz de Empleados y del contexto se manda a traer el metodo
        // getBean() para traer el bean que se creo en el archivo applicationContext.xml
        // Se manda a traer el bean "miDirectorEmpleado"
        Empleados Pablo = contexto.getBean("miDirectorEmpleado", Empleados.class);
        System.out.println(Pablo.getTareas());
        System.out.println(Pablo.getInforme());

        // Se manda a traer el bean "miDirectorEmpleado" atraves del objecto SecretarioEmpleado
        // Se tiene que poner el objecto DirectorEmpleado porque desde la interfaz no se llega a los metodos getters
        // y setters de email y nombre de la empresa
        DirectorEmpleado PabloDatos = contexto.getBean("miDirectorEmpleado", DirectorEmpleado.class);
        System.out.println("Email Pablo: " + PabloDatos.getEmail());
        System.out.println("Nombre de la Empresa: " + PabloDatos.getNombreEmpresa());

        System.out.println();

        // Se manda a traer el bean "miJefeEmpleado"
        Empleados Juan = contexto.getBean("miJefeEmpleado", Empleados.class);
        System.out.println(Juan.getTareas());
        System.out.println(Juan.getInforme());

        System.out.println();

        // Se manda a traer el bean "miSecretarioEmpleado" atraves de la interfaz de Empleados
        Empleados Maria = contexto.getBean("miSecretarioEmpleado", Empleados.class);
        System.out.println(Maria.getTareas());
        System.out.println(Maria.getInforme());

        // Se manda a traer el bean "miSecretarioEmpleado" atraves del objecto SecretarioEmpleado
        // Se tiene que poner el objecto SecretarioEmpleado porque desde la interfaz no se llega a los metodos getters
        // y setters de email y nombre de la empresa
        SecretarioEmpleado MariaDatos = contexto.getBean("miSecretarioEmpleado", SecretarioEmpleado.class);
        System.out.println("Email Maria: " + MariaDatos.getEmail());
        System.out.println("Nombre de la Empresa: " + MariaDatos.getNombreEmpresa());

        // Se crea otro objecto de SecretarioEmpleado con el nombre de clase PabloDatos
        SecretarioEmpleado FernandoDatos = contexto.getBean("miSecretarioEmpleado", SecretarioEmpleado.class);
        System.out.println("Email Fernando: " + FernandoDatos.getEmail());
        System.out.println("Nombre de la Empresa: " + FernandoDatos.getNombreEmpresa());

        contexto.close();

    }
}