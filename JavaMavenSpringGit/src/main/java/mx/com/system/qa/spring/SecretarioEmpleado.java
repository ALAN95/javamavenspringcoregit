package mx.com.system.qa.spring;

public class SecretarioEmpleado implements Empleados {

    private CreacionInformes nuevoInforme;
    private String email;
    private String nombreEmpresa;

    /*
        // Creación del contructor que inyecta la dependencia
        public SecretarioEmpleado(CreacionInformes nuevoInforme){
            this.nuevoInforme = nuevoInforme;
        }
    */

    // Creacion del metodo setter que inyecta la dependencia de la clase CreacionInformes
    public void setInformeNuevo(CreacionInformes nuevoInforme){
        this.nuevoInforme = nuevoInforme;
    }

    public String getTareas(){
        return "Gestionar la agenda de los Jefes";
    }

    public String getInforme() {
        return "Informe generado por el Secretario: " + nuevoInforme.getInforme();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }
}
