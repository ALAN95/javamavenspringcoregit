package mx.com.system.qa.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoDemoSingletonPrototype {
    public static void main(String[] args) {

        // Cargar de XML de configuracion
        ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("applicationContextSingletonPrototype.xml");

        // Peticion de Beans al contenedor de Spring
        Empleados Pedro = contexto.getBean("miSecretarioEmpleado", Empleados.class);
        System.out.println(Pedro.getTareas());
        System.out.println(Pedro.getInforme());
        System.out.println(Pedro); // Este Objecto apunta al mismo direccion de memoria del objecto Maria

        System.out.println();

        SecretarioEmpleado Maria = contexto.getBean("miSecretarioEmpleado", SecretarioEmpleado.class);
        System.out.println(Maria.getEmail());
        System.out.println(Maria.getNombreEmpresa());
        System.out.println(Maria);

        if(Pedro.equals(Maria)){
            System.out.println("Son iguales, Apuntan al mismo objecto");
        }else{
            System.out.println("No son iguales, No se trata del mismo objeto");
        }

    }
}
