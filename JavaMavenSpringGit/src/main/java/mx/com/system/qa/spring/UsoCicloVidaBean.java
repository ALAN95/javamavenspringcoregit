package mx.com.system.qa.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoCicloVidaBean {
    public static void main(String[] args) {
        // Cargar el xml de configuracion
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContexInitDestroy.xml");

        // Obtencion del Bean
        Empleados Juan = context.getBean("miDirectorEmpleado", Empleados.class);

        System.out.println(Juan.getInforme());

        // Cerrar el contexto
        context.close();

    }
}
